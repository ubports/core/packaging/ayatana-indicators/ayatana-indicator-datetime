ayatana-indicator-datetime (24.5.0-1ubports1) noble; urgency=medium

  * Merge version 24.5.0-1 from Debian unstable.

 -- Guido Berhoerster <guido+ubports@berhoerster.name>  Wed, 22 May 2024 13:43:09 +0200

ayatana-indicator-datetime (24.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 22 May 2024 08:59:13 +0200

ayatana-indicator-datetime (24.2.0-2) unstable; urgency=medium

  * debian/control:
    + Replace systemd by systemd-dev in B-D. (Closes: #1060548).
    + Bump Standards-Version to 4.7.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 14 May 2024 16:35:59 +0200

ayatana-indicator-datetime (24.2.0-1) unstable; urgency=medium

  [ Ratchanan Srirattanamet ]
  * debian/rules:
    + Fix building with nocheck build profile.

  [ Mike Gabriel ]
  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 12 Feb 2024 18:01:22 +0100

ayatana-indicator-datetime (23.6.0-1) unstable; urgency=medium

  * New upstream release.
    - Fix playing custom alarm sounds set up via lomiri-clock-app.
      (Closes: #1037330).
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 11 Jun 2023 16:33:56 +0200

ayatana-indicator-datetime (22.9.1-1ubports1) focal; urgency=medium

  * Merge version 22.9.1-1 from Debian unstable.

 -- Guido Berhoerster <guido+ubports@berhoerster.name>  Thu, 24 Nov 2022 08:31:40 +0100

ayatana-indicator-datetime (22.9.1-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 23 Nov 2022 17:06:05 +0100

ayatana-indicator-datetime (22.9.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.
  * debian/copyright:
    + Update copyright attributions for debian/*.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 01 Oct 2022 10:48:58 +0200

ayatana-indicator-datetime (22.2.0-1ubports1) focal; urgency=medium

  * Merge version 22.2.0-1 from Debian unstable.
  * Dropped change:
    - Add missing deps, as Debian re-organized B-Ds and added those already.
  * debian/ubports.source_location: update location for 22.2.0

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Fri, 04 Mar 2022 20:26:35 +0000

ayatana-indicator-datetime (22.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/control:
    + Add B-Ds: lomiri-schemas and lomiri-sounds.
    + Sort B-Ds.
    + Add B-D liblomiri-url-dispatcher-dev for enabled Lomiri features.
  * debian/rules:
    + Add build flag '-DENABLE_LOMIRI_FEATURES'.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 20 Feb 2022 20:41:40 +0100

ayatana-indicator-datetime (0.9.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/upstream/metadata:
    + Update points of contact, put UBports Foundation in Donation: field.
  * debian/rules:
    + Use upstream's NEWS file as upstream ChangeLog.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/control:
    + Update build requirements in B-D: field. Also add <!nocheck> flags.
    + Bump Standards-Version: 4.6.0. No changes needed.
  * debian/rules:
    + Enable unit tests.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 22 Nov 2021 21:24:11 +0100

ayatana-indicator-datetime (0.9.0-0) unstable; urgency=medium

  * New upstream release

 -- Marius Gripsgard <marius@ubports.com>  Mon, 22 Nov 2021 19:43:12 +0100

ayatana-indicator-datetime (0.8.3-1) unstable; urgency=medium

  * New upstream release. (Closes: #995279).
  * debian/patches:
    + Drop 0001_port-to-lomiri-url-dispatcher.patch. Shipped upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 19 Oct 2021 10:16:58 +0200

ayatana-indicator-datetime (0.8.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Add 0001_port-to-lomiri-url-dispatcher.patch. Port to Lomiri URL
      Dispatcher.
  * debian/control:
    + Fix wrong Github project in Homepage: field's URL. (Closes: #974742).
    + Fix capitalization of the word 'Xfce'.
    + Bump Standards-Version: to 4.5.1. No changes needed.
    + Add B-D: liblomiri-url-dispatcher-dev.
  * debian/watch:
    + Update format version to 4.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 03 Feb 2021 12:36:06 +0100

ayatana-indicator-datetime (0.8.1-2) unstable; urgency=medium

  * debian/control:
    + Add Debian UBports Team to Uploaders: field.
    + Mention desktop envs that support system indicators in LONG_DESCRIPTION.
    + Typo fix in LONG_DESCRIPTION.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 07 Nov 2020 15:09:09 +0100

ayatana-indicator-datetime (0.8.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add B:/R: relation to indicator datetime. (LP:#1893028, LP:#1893244).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 10 Sep 2020 22:14:06 +0200

ayatana-indicator-datetime (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Versioned D on ayatana-indicator-common (>= 0.8.0).
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 18 Aug 2020 10:08:16 +0200

ayatana-indicator-datetime (0.4.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 0001_CMakeLists.txt-Drop-workaround-for-missing-libexec-d.patch.
      Applied upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 04 Aug 2020 18:13:06 +0200

ayatana-indicator-datetime (0.4.0-1) unstable; urgency=medium

  * Initial release to Debian. (Closes: #966170).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 24 Jul 2020 12:13:39 +0200
